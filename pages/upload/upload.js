import WeCropper from '../../utils/we-cropper/we-cropper.js'
import { Router } from "../../utils/common.js";
import { RemoteDataService } from "../../utils/remoteDataService.js";

const device = wx.getSystemInfoSync()
const width = device.windowWidth
const height = device.windowHeight-43

Page({
  data: {
    cropperOpt: {
      id: 'cropper',
      width,
      height,
      scale: 2.5,
      zoom: 8,
      cut: {
        x: (width - 300) / 2,
        y: (height - 300) / 2,
        width: 300,
        height: 300
      }
    },
    pet: {},
    petVali: {}
  },
  touchStart (e) {
    this.wecropper.touchStart(e)
  },
  touchMove (e) {
    this.wecropper.touchMove(e)
  },
  touchEnd (e) {
    this.wecropper.touchEnd(e)
  },
  getCropperImage () {
    let that = this
    that.wecropper.getCropperImage((avatar) => {
      if (avatar) {
        wx.showToast({
          icon: "loading",
          title: "正在上传"
        })
        //  获取到裁剪后的图片，上传到服务器
        let params = {
          uploadType: "petAvatar"
        }
        wx.showNavigationBarLoading()
        RemoteDataService.uploadPetAvatar(params, avatar, 'avatarFile').then(result => {
          if(result && result.code == "000") {
            //put in transfer para
            console.log(that.data.pet)
            console.log(that.data.petVali)
            console.log(that.data.pet.avatar)
            that.data.pet.avatar = result.filePath
            that.data.pet.avatarTmp = avatar
            that.data.petVali.avatarOk = true
            console.log("ok")
            let params = {
              pet: JSON.stringify(that.data.pet),
              petVali: JSON.stringify(that.data.petVali)
            }
            console.log("Ready to return")
            console.log(params)
            Router.redirectTo("../dogedit/dogedit", params);
          }
          wx.hideNavigationBarLoading()
        }).catch(err => {
          wx.hideNavigationBarLoading()
        })
      } else {
        console.log('获取图片失败，请稍后重试')
      }
    })
  },
  uploadTap () {
    const self = this

    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success (res) {
        const src = res.tempFilePaths[0]
        //  获取裁剪图片资源后，给data添加src属性及其值
        console.log("res.tempFiles[0].size==", res.tempFiles[0].size);
        self.wecropper.pushOrign(src)
      }
    })
  },
  onLoad (option) {
    const { cropperOpt } = this.data
    console.log(option.pet)
    console.log(option.petVali)
    if (option.pet){
      this.data.pet = JSON.parse(option.pet)
    }
    if (option.petVali){
      this.data.petVali = JSON.parse(option.petVali)
    }
    console.log(this.data.pet)
    console.log(this.data.petVali)
    if (option.src) {
      cropperOpt.src = option.src
      console.log("height===" + cropperOpt.height);
      new WeCropper(cropperOpt)
        .on('ready', (ctx) => {
          console.log('wecropper is ready for work!')
        })
        .on('beforeImageLoad', (ctx) => {
          console.log('before picture loaded, i can do something')
          console.log('current canvas context:', ctx)
          wx.showToast({
            title: '上传中',
            icon: 'loading',
            duration: 20000
          })
        })
        .on('imageLoad', (ctx) => {
          console.log('picture loaded')
          console.log('current canvas context:', ctx)
          wx.hideToast()
        })
        .on('beforeDraw', (ctx, instance) => {
          console.log('before canvas draw,i can do something')
          console.log('current canvas context:', ctx)
        })
        .updateCanvas()
      console.log("height===" + cropperOpt.height);
    }
  }
})
